unit ConfigForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, IniFiles, AudioStream, AUdioStreamFrame;

type
  TForm1 = class(TForm)
    gbStreams: TGroupBox;
    btnAdd: TButton;
    edtStreamName: TEdit;
    edtMusicFile: TEdit;
    cmbDevice: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Button2: TButton;
    btnOk: TButton;
    btnCancel: TButton;
    OpenDialog1: TOpenDialog;
    procedure btnAddClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    FStreams : array of TAudioStream;
    { Private declarations }
  protected
    procedure WndProc( var message : TMessage ); override;
  public
    procedure ReadFromIni( AIni : TIniFile );
    procedure WriteToIni( AIni : TIniFile );

    procedure Setup;
    procedure UpdateControls;

    { Public declarations }

  end;

var
  Form1: TForm1;

function Config( var AStreams : TAudioStreamsArray ) : Boolean;

implementation

{$R *.dfm}

function Config( var AStreams : TAudioStreamsArray ) : Boolean;
var
  Form1: TForm1;
  Ini : TIniFile;
begin
  Form1 := TForm1.Create(Application);
  Ini:= TIniFile.Create( ExtractFilePath( Application.ExeName ) + '\Config.ini' );
  try
    Form1.ReadFromIni( Ini );
    Form1.Setup;
    if Form1.ShowModal = mrOk then begin
      Form1.WriteToIni( Ini );
      Result:= True;
      AStreams:= TAudioStreamsArray( Form1.FStreams );
    end else
      Result:= False;
  finally
    Form1.Free;
    Ini.Free;
  end;
end;

procedure TForm1.btnAddClick(Sender: TObject);
var
  I : Integer;
begin
  if edtStreamName.Text = '' then begin
    ShowMessage( 'Stream should have name' );
    Exit;
  end;
  for I := 0 to Length( FStreams ) - 1 do
    if FStreams[I].StreamName = edtStreamName.Text then begin
      ShowMessage( 'A stream with that name already exists' );
      Exit;
    end;
  if not FileExists( edtMusicFile.Text ) then begin
    ShowMessage( 'No music file loaded' );
    Exit;
  end;

  if not ( LowerCase( ExtractFileExt( edtMusicFile.Text ) ) = '.wav' ) then begin
    ShowMessage( 'Only wav files supported.' );
    Exit;
  end;

  SetLength( FStreams, Length( FStreams ) + 1 );
  FStreams[ Length(FStreams) - 1 ].StreamName:= edtStreamName.Text;
  FStreams[ Length(FStreams) - 1 ].OutputDeviceName:= cmbDevice.Items[cmbDevice.ItemIndex];
  FStreams[ Length(FStreams) - 1 ].AudioFile:= edtMusicFile.Text;
  FStreams[ Length(FStreams) - 1 ].Enabled:= True;

  UpdateControls;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  OpenDialog1.Filter:= '*.WAV';
  if OpenDialog1.Execute( Self.Handle ) then begin
    edtMusicFile.Text:= OpenDialog1.Files[0];
  end;
end;

procedure TForm1.ReadFromIni(AIni: TIniFile);
var
  StreamNames : TStringList;
  I: Integer;
  StreamInfo : TAudioStream;
begin
  StreamNames:= TStringList.Create;
  try
    AIni.ReadSection( 'Streams', StreamNames );
    for I := 0 to StreamNames.Count - 1 do begin
      with StreamInfo do begin
        StreamName:= StreamNames[I];
        OutputDeviceName:= AIni.ReadString( StreamName, 'DeviceName', '' );
        AudioFile:= Aini.ReadString( StreamName, 'AudioFile', '' );
        Enabled:= AIni.ReadBool( StreamName, 'Enabled', False );
      end;
      SetLength( FStreams, I + 1 );
      FStreams[I]:= StreamInfo;
    end;
  finally
    StreamNames.Free;
  end;
end;

procedure TForm1.Setup;
var
  I: Integer;
  Devices : TDeviceInfoArray;
begin
  UpdateControls;

  Devices:= PLaybackDevices;
  cmbDevice.Items.Clear;
  for I := 0 to Length( Devices ) - 1 do
    cmbDevice.Items.Add( Devices[I].Name );
  if cmbDevice.Items.Count > 0 then
    cmbDevice.ItemIndex:= 0;
end;




procedure TForm1.UpdateControls;
var
  AudioStreamFrame : TAudioStreamFram;
  I: Integer;
const
  MARGIN = 10;
begin
  for I := 0 to gbStreams.ControlCount - 1 do
    gbStreams.RemoveControl( gbStreams.Controls[0] );
  for I := 0 to Length( FStreams ) - 1 do begin
    AUdioStreamFrame:= TAudioStreamFram.Create( Self );
    AUdioStreamFrame.Name:= UniqueComponentName( AUdioStreamFrame, FStreams[I].StreamName );
    AudioStreamFrame.Parent:= gbStreams;
    AUdioStreamFrame.Setup( FStreams[I] );
    AudioStreamFrame.Top:= I * AUdioStreamFrame.Height + MARGIN;
    if AudioStreamFrame.Top + AudioStreamFrame.Height > gbStreams.Height - MARGIN then begin
      Self.Height:= Self.Height + ( AudioStreamFrame.Top + AudioStreamFrame.Height - gbStreams.Height + MARGIN);
    end;

    AudioStreamFrame.Left:= 0;
  end;
end;

procedure TForm1.WndProc(var message: TMessage);
var
  X : Integer;
  function FindStreamIndex( AName : string ) : Integer;
  var
    I: Integer;
  begin
    Result:= -1;
    for I := 0 to Length( FStreams ) - 1 do
      if FStreams[I].StreamName = AName then
        Result:= I;
  end;
begin
  inherited;
  if message.Msg = WM_STREAM_CHANGED then begin
    X:= FindStreamIndex(PChar( message.WParam ));
    if X < 0  then Exit;
    if Pointer( message.LParam ) = nil then begin
      FStreams[X]:= FStreams[ Length(FStreams) - 1 ];
      SetLength( FStreams, Length( FStreams ) - 1 );
      UpdateControls;
    end else begin
      FStreams[X]:= PAudioStream( message.LParam )^;
    end;
  end;

end;

procedure TForm1.WriteToIni(AIni: TIniFile);
var
  I: Integer;
  SL : TStringList;
begin
  SL:= TStringList.Create;
  try
    Aini.ReadSection( 'Streams', SL );
    for I := 0 to SL.Count - 1 do
      AIni.EraseSection( SL[I] );
    AIni.EraseSection( 'Streams' );
    for I := 0 to Length( FStreams ) - 1 do begin
      AIni.WriteString( 'Streams', FStreams[I].StreamName, '' );
      Aini.WriteString( FStreams[I].StreamName, 'DeviceName', FStreams[I].OutputDeviceName );
      Aini.WriteString( FStreams[I].StreamName, 'AudioFile', FStreams[I].AudioFile );
      Aini.WriteBool( FStreams[I].StreamName, 'Enabled', FStreams[I].Enabled );
    end;
  finally

  end;

end;

end.

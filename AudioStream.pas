unit AudioStream;

interface
uses
  System.SysUtils, Vcl.Forms, mmSystem, WaveUtils, WaveStorage, Vcl.StdCtrls, WaveIO, WaveOut,
  WavePlayers, Vcl.Controls, System.Classes, VCl.ExtCtrls;

  type
    TplayerStatus = (psPlaying, psStopped, psPaused);
    TDeviceInfo = record
      Name : String;
      ID : Integer;
    end;

    TAudioStream = record
      StreamName : string;
      OutputDeviceName : string;
      AudioFile : string;
      Enabled : Boolean;
    end;
    PAudioStream = ^TAudioStream;

    TDextroAudioPlayer = class
      procedure OnTimer(Sender: TObject);
    private
      FAudioStream : TAudioStream;
      FWavePlayer : TStockAudioPlayer;
      FVolume : Integer;
      FStopped : Boolean;
      FTimer : TTimer;
      function GetPlayerStatus: TPlayerStatus;
    procedure SetVolume(const Value: Integer);
    function GetVolume: Integer;
     public
      constructor Create( AAudioStream : TAudioStream );
      destructor Destroy; override;

      function Play : Boolean;
      function Pause : Boolean;
      function Stop : Boolean;

      property AudioStream : TAudioStream read FAudioStream;
      property Volume : Integer read GetVolume write SetVolume;
      property PlayerStatus : TPlayerStatus read GetPlayerStatus;
    end;

    TDeviceInfoArray = array of TDeviceInfo;
    TAudioStreamsArray = array of TAudioStream;

function PLaybackDevices : TDeviceInfoArray;
function DeviceIdForDeviceName( AName : string ) : Integer;

function UniqueComponentName( AComponent : TComponent; const AName : string ) : string;

implementation
uses
  Winapi.Windows;


function PLaybackDevices : TDeviceInfoArray;
var
  Count : Integer;
  DummyPLayer : TStockAudioPlayer;
begin
  Count:= 0;
  DummyPLayer:= TStockAudioPlayer.Create( nil );
  try
    while True do begin
      try
        DummyPLayer.DeviceID:= Count;
      except
        break;
      end;
      Inc( Count );
      SetLength( Result, Count );
      Result[Count - 1].Name:= DummyPLayer.DeviceName;
      Result[Count - 1].ID:= DummyPLayer.DeviceID;
    end;
  finally
    FreeAndNil( DummyPLayer );
  end;
end;

function DeviceIdForDeviceName( AName : string ) : Integer;
var
  I: Integer;
  Devices : TDeviceInfoArray;
begin
  Result:= -1;
  Devices:= PlaybackDevices;
  for I := 0 to Length( Devices ) - 1 do begin
    if AName = Devices[ I ].Name then
      Result:= I;
  end;
end;


function UniqueComponentName( AComponent : TComponent; const AName : string ) : string;
var
  I : Integer;
begin
  Result := AName;
  I := 1;

  if(( AComponent = nil ) or ( AComponent.Owner = nil )) then begin
    while ( not IsUniqueGlobalComponentName( Result )) do begin
      Result := Format( '%s_%d', [ AName, I ]);
      Inc( I );
    end;
  end
  else begin
    while ( AComponent.Owner.FindComponent( Result ) <> nil ) do begin
      Result := Format( '%s_%d', [ AName, I ]);
      Inc( I );
    end;
  end;
end;

{ TDextroAudioPlayer }

constructor TDextroAudioPlayer.Create( AAudioStream: TAudioStream);
var
  Id : Integer;
begin
  FAudioStream:= AAudioStream;
  FWavePlayer:= TStockAudioPlayer.Create( nil );
  FWavePlayer.Options:= [woSetVolume];
  Id:= DeviceIdForDeviceName( FAudioStream.OutputDeviceName );
  if Id = -1 then raise Exception.Create('No such device.');
  FWavePlayer.DeviceID:= Id;
  FWavePlayer.Async:= True;
  FVolume:= 50;
  FStopped:= True;
  FTimer:= TTimer.Create( nil );
  FTimer.Enabled:= False;
  FTimer.Interval:= 500;
  FTimer.OnTimer:= OnTimer;
end;

destructor TDextroAudioPlayer.Destroy;
begin
  FWavePlayer.Active:= False;
  FreeAndNil( FWavePlayer );
  FreeAndNil( Ftimer );
  inherited;
end;

function TDextroAudioPlayer.GetPlayerStatus: TPlayerStatus;
begin
  if not FStopped then
    if FWavePlayer.Paused then
      Result:= psPaused
    else
      Result:= psPlaying
  else
    Result:= psStopped;
end;

function TDextroAudioPlayer.GetVolume: Integer;
begin
  Result:= FWavePlayer.Volume;
end;

function TDextroAudioPlayer.Pause: Boolean;
begin
  if not FWavePlayer.Active then begin
    Result:= False;
    Exit;
  end;
  FWavePlayer.Paused:= not FWavePlayer.Paused;
  Result:= True;
end;

function TDextroAudioPlayer.Play: Boolean;
begin
  if FWavePlayer.Active then begin
    Result:= False;
    Exit;
  end;
  FWavePlayer.PlayFile( FAudioStream.AudioFile );
  Result:= True;
  FStopped:= False;
  FTimer.Enabled:= True;
end;

procedure TDextroAudioPlayer.OnTimer(Sender: TObject);
begin
  if not FStopped and not FWavePlayer.Paused then begin
    if FWavePlayer.Position <= 0 then begin
      FWavePlayer.Active:= False;
      FWavePlayer.PlayFile( FAudioStream.AudioFile );
    end;
  end;
end;

procedure TDextroAudioPlayer.SetVolume(const Value: Integer);
begin
  FWavePlayer.Volume:= Value;
end;

function TDextroAudioPlayer.Stop: Boolean;
begin
  try
    Result:= FWavePlayer.Stop;
  except on E : EWaveAudioInvalidOperation do
    Result:= True;//Already stopped
  end;
  if Result then begin
    FStopped:= True;
    FWavePlayer.Paused:= False;
    FTimer.Enabled:= False;
  end;

end;

end.

program DextrophobiaMultiStreamAudio;

uses
  Vcl.Forms,
  Main in 'Main.pas' {MainForm},
  ConfigForm in 'ConfigForm.pas' {Form1},
  AudioStream in 'AudioStream.pas',
  AudioStreamFrame in 'AudioStreamFrame.pas' {AudioStreamFram: TFrame},
  DextroPlayerFrame in 'DextroPlayerFrame.pas' {DxtPlayerFrame: TFrame};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.

object AudioStreamFram: TAudioStreamFram
  Left = 0
  Top = 0
  Width = 450
  Height = 31
  TabOrder = 0
  object Label1: TLabel
    Left = 115
    Top = 8
    Width = 3
    Height = 13
  end
  object cbStreamName: TCheckBox
    Left = 3
    Top = 7
    Width = 97
    Height = 17
    TabOrder = 0
    OnClick = cbStreamNameClick
  end
  object btnRemove: TButton
    Left = 372
    Top = 3
    Width = 75
    Height = 25
    Caption = 'Remove'
    TabOrder = 1
    OnClick = btnRemoveClick
  end
end

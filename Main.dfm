object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'Audio Streams'
  ClientHeight = 299
  ClientWidth = 504
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    504
    299)
  PixelsPerInch = 96
  TextHeight = 13
  object btnConfig: TButton
    Left = 8
    Top = 266
    Width = 75
    Height = 25
    Caption = 'Config'
    TabOrder = 0
    OnClick = btnConfigClick
  end
  object Button1: TButton
    Left = 340
    Top = 266
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Play All'
    TabOrder = 1
    OnClick = Button1Click
  end
  object btnStopAll: TButton
    Left = 421
    Top = 266
    Width = 75
    Height = 25
    Caption = 'Stop All'
    TabOrder = 2
    OnClick = btnStopAllClick
  end
  object Timer1: TTimer
    Left = 248
    Top = 152
  end
end

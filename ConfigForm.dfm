object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Configure audio streams'
  ClientHeight = 283
  ClientWidth = 469
  Color = clBtnFace
  Constraints.MaxWidth = 485
  Constraints.MinWidth = 485
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    469
    283)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 202
    Width = 31
    Height = 13
    Anchors = [akRight, akBottom]
    Caption = 'Name:'
    ExplicitTop = 170
  end
  object Label2: TLabel
    Left = 103
    Top = 202
    Width = 49
    Height = 13
    Anchors = [akRight, akBottom]
    Caption = 'Music File:'
    ExplicitTop = 170
  end
  object Label3: TLabel
    Left = 284
    Top = 200
    Width = 36
    Height = 13
    Anchors = [akRight, akBottom]
    Caption = 'Device:'
    ExplicitTop = 168
  end
  object gbStreams: TGroupBox
    Left = 8
    Top = 13
    Width = 453
    Height = 178
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = 'Streams'
    TabOrder = 0
  end
  object btnAdd: TButton
    Left = 408
    Top = 218
    Width = 53
    Height = 21
    Anchors = [akRight, akBottom]
    Caption = 'Add'
    TabOrder = 1
    OnClick = btnAddClick
  end
  object edtStreamName: TEdit
    Left = 8
    Top = 218
    Width = 89
    Height = 21
    Anchors = [akRight, akBottom]
    TabOrder = 2
  end
  object edtMusicFile: TEdit
    Left = 103
    Top = 218
    Width = 150
    Height = 21
    Anchors = [akRight, akBottom]
    TabOrder = 3
  end
  object cmbDevice: TComboBox
    Left = 284
    Top = 218
    Width = 119
    Height = 21
    Anchors = [akRight, akBottom]
    TabOrder = 4
  end
  object Button2: TButton
    Left = 257
    Top = 218
    Width = 22
    Height = 21
    Anchors = [akRight, akBottom]
    Caption = '...'
    TabOrder = 5
    OnClick = Button2Click
  end
  object btnOk: TButton
    Left = 386
    Top = 250
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 6
  end
  object btnCancel: TButton
    Left = 305
    Top = 250
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 7
  end
  object OpenDialog1: TOpenDialog
    Left = 176
    Top = 272
  end
end

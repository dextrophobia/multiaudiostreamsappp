unit Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, mmSystem, WaveUtils, WaveStorage, Vcl.StdCtrls, WaveIO,
  WaveOut, WavePlayers, ConfigForm, AudioStream, IniFiles, DextroPlayerFrame, Vcl.ExtCtrls;

type
  TMainForm = class(TForm)
    btnConfig: TButton;
    Button1: TButton;
    btnStopAll: TButton;
    Timer1: TTimer;
    procedure btnConfigClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure btnStopAllClick(Sender: TObject);

  private
    FStreams : array of TAudioStream;
    FPlayers : TList;

    procedure Setup;
    procedure UpdateControls;
    procedure UpdateButtons;

    procedure ReadVolumes( AIni : TIniFile );
    procedure ReadFromIni(AIni: TIniFile);
    procedure WriteVolumes( AIni : TIniFIle );
    { Private declarations }
  protected
    procedure WndProc( var message : TMessage ); override;
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

procedure TMainForm.btnConfigClick(Sender: TObject);
var
  Streams : TAudioStreamsArray;
  I : Integer;
  Volumes : TStringLIst;
begin
  Volumes:= TStringList.Create;
  try
    for I := 0 to FPlayers.Count - 1 do
      Volumes.AddObject( TDxtPlayerFrame( FPlayers[I] ).lblName.Caption, Pointer( TDxtPlayerFrame( FPlayers[I] ).Player.Volume ) );
    if Config( Streams ) then begin
      SetLength( FStreams, 0 );
      for I := 0 to Length( Streams ) - 1 do
        if Streams[I].Enabled then begin
          SetLength( FStreams, Length( FStreams ) + 1);
          FStreams[Length( FStreams ) - 1]:= Streams[I];
        end;
      UpdateControls;
    end;
    for I := 0 to FPlayers.Count - 1 do begin 
      if Volumes.IndexOf( FStreams[I].StreamName ) >= 0 then
        TDxtPlayerFrame( FPlayers[I] ).TrackBar1.Position:= Integer( Volumes.Objects[I] );        
    end;
  finally
    Volumes.Free;
  end;
end;

procedure TMainForm.Setup;
var
  Ini : TIniFile;
begin
  Ini := TIniFile.Create( ExtractFilePath( Application.ExeName ) + '\Config.ini');
  try
    ReadFromIni( Ini );
  finally
    Ini.Free;
  end;
  UpdateControls;
  Ini := TIniFile.Create( ExtractFilePath( Application.ExeName ) + '\Config.ini');
  try
    ReadVolumes( Ini );
  finally
    Ini.Free;
  end;
end;


procedure TMainForm.UpdateControls;
var
  I: Integer;
  PlayerFrame : TDxtPlayerFrame;
  S : String;

  function _ValidateStreams : string;
  var
    X, Y : Integer;
  begin
    Result:= '';
    for X := 0 to Length( FStreams ) - 1 do begin
      for Y := 0 to Length( FStreams ) - 1 do begin
        if ( X <> Y  ) and ( FStreams[X].OutputDeviceName = FStreams[Y].OutputDeviceName ) then begin
          Result:= 'There are two stream using the same output device. Please configure again.'
        end;
      end;
      if DeviceIdForDeviceName( FStreams[X].OutputDeviceName ) = -1 then begin
        Result:= FStreams[X].StreamName + '  has invalid output device. Please configure again.';
        Exit;
      end;
    end;
  end;

const
  MARGIN = 10;
begin
  S:= _ValidateStreams;
  if S <> '' then begin
    ShowMessage( S );
    Exit;
  end;

  for I := 0 to FPlayers.Count - 1 do
    RemoveControl( TDxtPlayerFrame( FPlayers[I] ) );
  FPlayers.Clear;

  for I := 0 to Length( FStreams ) - 1 do begin
    PlayerFrame:= TDxtPlayerFrame.Create( Self );
    PlayerFrame.Name:= UniqueComponentName( PlayerFrame, FStreams[I].StreamName );
    PlayerFrame.Parent:= Self;
    PlayerFrame.Init( FStreams[I] );
    PlayerFrame.Top:= I * PlayerFrame.Height + MARGIN;
    PlayerFrame.Left:= MARGIN;
    if PlayerFrame.Top + PlayerFrame.Height > Self.Height - MARGIN then
      Self.Height:= Self.Height + ( PlayerFrame.Top + PlayerFrame.Height - Self.Height + MARGIN);
    FPlayers.Add( PlayerFrame );
  end;

  UpdateButtons;
end;

procedure TMainForm.UpdateButtons;
var
  I : Integer;
  _ : Boolean;
begin
  for I := 0 to FPlayers.Count - 1 do
    if not TDxtPlayerFrame( FPlayers[ I ] ).PlayAvailable then
      break;
  _:= ( I = FPlayers.Count );
  Button1.Enabled:= _;
  btnConfig.Enabled:= _ or ( FPlayers.Count = 0 );
  btnStopAll.Enabled:= ( FPlayers.Count <> 0 );
end;

procedure TMainForm.WndProc(var message: TMessage);
begin
  if message.Msg = WM_PLAYER_CHANGED then
    UpdateButtons;
  inherited;
end;

procedure TMainForm.WriteVolumes(AIni: TIniFIle);
var
  I: Integer;
begin
  for I := 0 to FPlayers.Count - 1 do begin
    AIni.WriteInteger( FStreams[I].StreamName, 'Volume', TDxtPlayerFrame( FPlayers[I] ).Player.Volume );
  end;
end;

procedure TMainForm.btnStopAllClick(Sender: TObject);
var
  I : Integer;
begin
  for I := 0 to FPlayers.Count - 1 do
    TDxtPlayerFrame( FPlayers[I] ).Stop;
  UpdateButtons;
end;

procedure TMainForm.Button1Click(Sender: TObject);
var
  I: Integer;
begin
  for I := 0 to FPlayers.Count - 1 do
    TDxtPlayerFrame( FPlayers[I] ).Play;
  UpdateButtons;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  FPlayers:= Tlist.Create;
  Setup;
end;

procedure TMainForm.FormDestroy(Sender: TObject);
var
  Ini : TiniFile;
begin
  Ini := TIniFile.Create( ExtractFilePath( Application.ExeName ) + '\Config.ini');
  try
    WriteVolumes( Ini );
  finally
    Ini.Free;
  end;
  FPlayers.Free;
end;

procedure TMainForm.ReadFromIni(AIni: TIniFile);
var
  StreamNames : TStringList;
  I: Integer;
  StreamInfo : TAudioStream;
begin
  StreamNames:= TStringList.Create;
  try
    AIni.ReadSection( 'Streams', StreamNames );
    for I := 0 to StreamNames.Count - 1 do begin
      with StreamInfo do begin
        StreamName:= StreamNames[I];
        OutputDeviceName:= AIni.ReadString( StreamName, 'DeviceName', '' );
        AudioFile:= Aini.ReadString( StreamName, 'AudioFile', '' );
        Enabled:= AIni.ReadBool( StreamName, 'Enabled', False );
      end;
      if StreamInfo.Enabled then begin
        SetLength( FStreams, I + 1 );
        FStreams[I]:= StreamInfo;
      end;
    end;
  finally
    StreamNames.Free;
  end;
end;

procedure TMainForm.ReadVolumes(AIni: TIniFile);
var
  I: Integer;
begin
  for I := 0 to Length( FStreams ) - 1 do
    TDxtPlayerFrame( FPlayers[I] ).TrackBar1.Position:= AIni.ReadInteger( FStreams[I].StreamName, 'Volume', 75 );
end;

end.

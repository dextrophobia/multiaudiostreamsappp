unit AudioStreamFrame;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, AudioStream, IniFiles;

type
  TAudioStreamFram = class(TFrame)
    Label1: TLabel;
    cbStreamName: TCheckBox;
    btnRemove: TButton;
    procedure cbStreamNameClick(Sender: TObject);
    procedure btnRemoveClick(Sender: TObject);
  private
    FAudioStream : TAudioStream;
    function IsActive: Boolean;
    function GetStreamName: string;
    { Private declarations }
  public
    procedure Setup( AAudioStream : TAudioStream );
    property Active : Boolean read IsActive;
    property StreamName : string read GetStreamName;
    { Public declarations }
  end;

const
  WM_STREAM_CHANGED = WM_USER + $1000;

implementation

{$R *.dfm}

{ TlblDeviceName }

procedure TAudioStreamFram.btnRemoveClick(Sender: TObject);
var
  P : PChar;
begin
  P:= StrNew( PChar( cbStreamName.Caption ) );
  SendMessage( TWinControl( Owner ).Handle, WM_STREAM_CHANGED, lparam( P ), wparam( nil ) );
  StrDispose( P );
end;

procedure TAudioStreamFram.cbStreamNameClick(Sender: TObject);
var
  P : PChar;
begin
  FAudioStream.Enabled:= cbStreamName.Checked;
  Label1.Enabled:= cbStreamName.Checked;
  P:= StrNew( PChar( cbStreamName.Caption ) );
  SendMessage( TWinControl( Owner ).Handle, WM_STREAM_CHANGED, lparam( P ), wparam( @FAudioStream ) );
  StrDispose( P );
end;

function TAudioStreamFram.GetStreamName: string;
begin
  Result:= FAudioStream.StreamName;
end;

function TAudioStreamFram.IsActive: Boolean;
begin
  Result:= cbStreamName.Checked;
end;


procedure TAudioStreamFram.Setup(AAudioStream: TAudioStream);
begin
  cbStreamName.Caption:= AAudioStream.StreamName;
  Label1.Caption:= AAudioStream.OutputDeviceName;
  FAudioStream:= AAudioStream;
  cbStreamName.Checked:= AAudioStream.Enabled;

end;


end.

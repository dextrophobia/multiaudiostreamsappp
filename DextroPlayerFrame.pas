unit DextroPlayerFrame;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, AudioStream, Vcl.StdCtrls, Vcl.Samples.Spin,
  Vcl.ComCtrls;

type
  TDxtPlayerFrame = class(TFrame)
    lblName: TLabel;
    btnPause: TButton;
    btnPlay: TButton;
    btnStop: TButton;
    TrackBar1: TTrackBar;
    procedure btnPlayClick(Sender: TObject);
    procedure btnPauseClick(Sender: TObject);
    procedure btnStopClick(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
//    procedure SpinEdit1Change(Sender: TObject);
  private
    FPlayer : TDextroAudioPlayer;
    { Private declarations }
  public
    { Public declarations }
    procedure Invalidat( AInformParent : Boolean = True );
    procedure Init( AAudioStream : TAudioStream );
    procedure Done;

    procedure Play;
    procedure Stop;
    function PlayAvailable : Boolean;

    property Player : TDextroAudioPlayer read FPlayer;
  end;
const
  WM_PLAYER_CHANGED = WM_USER + $1001;

implementation

{$R *.dfm}

{ TDxtPlayerFrame }

procedure TDxtPlayerFrame.btnPauseClick(Sender: TObject);
begin
  FPlayer.Pause;
  Invalidat;
end;

procedure TDxtPlayerFrame.btnPlayClick(Sender: TObject);
begin
  Play;
end;

procedure TDxtPlayerFrame.btnStopClick(Sender: TObject);
begin
  Stop;
end;


procedure TDxtPlayerFrame.Done;
begin
  FPlayer.Free;
end;

procedure TDxtPlayerFrame.Init(AAudioStream: TAudioStream);
begin
  FPlayer:= TDextroAudioPlayer.Create( AAudioStream );
  lblName.Caption:= AAudioStream.StreamName;
  TrackBar1.Position:= FPlayer.Volume;
  Invalidat( False );
end;

procedure TDxtPlayerFrame.Invalidat( AInformParent : Boolean = True );
begin
  case FPlayer.PlayerStatus of
    psPlaying : begin
      btnPlay.Enabled:= False;
      btnPause.Enabled:= True;
      btnStop.Enabled:= True;
    end;
    psStopped : begin
      btnPlay.Enabled:= True;
      btnPause.Enabled:= False;
      btnStop.Enabled:= False;
    end;
    psPaused : begin
      btnPlay.Enabled:= False;
      btnPause.Enabled:= True;
      btnStop.Enabled:= False;
    end;
  end;
  if AInformParent then
    SendMessage( Parent.Handle, WM_PLAYER_CHANGED, lparam( nil ), wparam( nil ) );
end;


procedure TDxtPlayerFrame.Play;
begin
  FPlayer.Play;
  Invalidat;
end;

function TDxtPlayerFrame.PlayAvailable: Boolean;
begin
  Result:= btnPlay.Enabled;
end;

//procedure TDxtPlayerFrame.SpinEdit1Change(Sender: TObject);
//begin
//  FPlayer.Volume:= SpinEdit1.Value;
//end;

procedure TDxtPlayerFrame.Stop;
begin
  FPlayer.Stop;
  Invalidat;
end;

procedure TDxtPlayerFrame.TrackBar1Change(Sender: TObject);
begin
  FPlayer.Volume:= TrackBar1.Position;
end;

end.

object DxtPlayerFrame: TDxtPlayerFrame
  Left = 0
  Top = 0
  Width = 489
  Height = 36
  TabOrder = 0
  DesignSize = (
    489
    36)
  object lblName: TLabel
    Left = 7
    Top = 8
    Width = 3
    Height = 13
  end
  object btnPause: TButton
    Left = 327
    Top = 3
    Width = 75
    Height = 25
    Anchors = [akRight]
    Caption = 'Pause'
    TabOrder = 0
    OnClick = btnPauseClick
  end
  object btnPlay: TButton
    Left = 246
    Top = 3
    Width = 75
    Height = 25
    Anchors = [akRight]
    Caption = 'Play'
    TabOrder = 1
    OnClick = btnPlayClick
  end
  object btnStop: TButton
    Left = 407
    Top = 3
    Width = 75
    Height = 25
    Anchors = [akRight]
    Caption = 'Stop'
    TabOrder = 2
    OnClick = btnStopClick
  end
  object TrackBar1: TTrackBar
    Left = 168
    Top = 3
    Width = 78
    Height = 40
    Max = 100
    TabOrder = 3
    OnChange = TrackBar1Change
  end
end
